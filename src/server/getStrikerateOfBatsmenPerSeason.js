const getStrikerateOfBatsmenPerSeason = (matches, deliveries) => {

    let currentSeason;
    let playerScoreDetails = {}
    let strikeRateDetails = {}

    deliveries.forEach((delivery) => {
        let player = delivery.batsman
        for (let i = 0; i < matches.length - 1; i++) {
            if (delivery.match_id === matches[i].id) {
                currentSeason = matches[i].season
            }
        }

        if (!playerScoreDetails.hasOwnProperty(player)) {
            playerScoreDetails[player] = {}
            if (!playerScoreDetails[player].hasOwnProperty(currentSeason)) {
                playerScoreDetails[player][currentSeason] = { "runsScored": 0, "ballsFaced": 0 }
                playerScoreDetails[player][currentSeason]["runsScored"] += parseInt(delivery.batsman_runs);

                if (parseInt(delivery.wide_runs) === 0) {
                    playerScoreDetails[player][currentSeason]["ballsFaced"] += 1
                }
            }
        }

        else {
            if (!playerScoreDetails[player].hasOwnProperty(currentSeason)) {
                playerScoreDetails[player][currentSeason] = { runsScored: 0, ballsFaced: 0 }
                playerScoreDetails[player][currentSeason]["runsScored"] += parseInt(delivery.batsman_runs);
                if (parseInt(delivery.wide_runs) === 0) {
                    playerScoreDetails[player][currentSeason]["ballsFaced"] += 1

                }
            }
            else {
                playerScoreDetails[player][currentSeason]["runsScored"] += parseInt(delivery.batsman_runs);
                if (parseInt(delivery.wide_runs) === 0) {
                    playerScoreDetails[player][currentSeason]["ballsFaced"] += 1

                }
            }
        }
    })

    for (let player in playerScoreDetails) {

        playerDetails = playerScoreDetails[player]
        strikeRateDetails[player] = {}
        for (let year in playerDetails) {
            scoreDetails = playerDetails[year]

            if (scoreDetails["ballsFaced"] !== 0) {
                strikeRate = parseFloat(((scoreDetails["runsScored"] / scoreDetails["ballsFaced"]) * 100).toFixed(3))
            }

            strikeRateDetails[player][year] = strikeRate
        }
    }
    let strikeRateArray = Object.entries(strikeRateDetails)
    strikeRateArray.sort()
    let sortedObject = Object.fromEntries(strikeRateArray)
    return (sortedObject)
}

module.exports = getStrikerateOfBatsmenPerSeason