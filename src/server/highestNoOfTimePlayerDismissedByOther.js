const highestNoOftimePlayerDismissedByOther = (deliveries) => {
    let validDismissals = ["caught", "stumped", "caught and bowled", "bowled", "hit wicket", "lbw"]

    let obj = {}
    const validationCheck = (delivery) => {
        return (validDismissals.includes(delivery.dismissal_kind))
    }


    deliveries.filter(delivery => {
        return validationCheck(delivery)
    })
        .forEach(delivery => {
            let bowler = delivery.bowler
            let batsman = delivery.player_dismissed
            let key = `Bowler: ${bowler} Batsman: ${batsman}`


            if (!obj.hasOwnProperty(key)) {
                obj[key] = 1
            }
            else {
                obj[key] += 1

            }

        })

    let detailsArray = Object.entries(obj)
    detailsArray.sort((a, b) => b[1] - a[1])

    let result = Object.fromEntries([detailsArray[0]])
    return result

}
module.exports = highestNoOftimePlayerDismissedByOther