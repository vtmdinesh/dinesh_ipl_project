const findNoOfTimesTeamWonTossAndGame = (matches) => {

    const matchCount = {}
    matches.filter(match => {
        return match.toss_winner === match.winner
    }).forEach(match => {
        let winner = match.winner
        if (!matchCount.hasOwnProperty(winner)) {
            matchCount[winner] = 1
        }
        else {
            matchCount[winner] += 1
        }

    })

    let arr = Object.entries(matchCount)
    let sortedObject = {}

    arr.sort((a, b) => b[1] - a[1])
    arr.forEach(team => {
        sortedObject[team[0]] = team[1]
    })

    return sortedObject
}

module.exports = findNoOfTimesTeamWonTossAndGame