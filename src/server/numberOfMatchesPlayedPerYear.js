const numberOfMatchesPlayedPerYear = (matches) => {
  let matchesCountPerYear = {};

  matches.forEach((match) => {
    let season = match.season;

    if (matchesCountPerYear[season] === undefined) {
      matchesCountPerYear[season] = 1;
    } else {
      matchesCountPerYear[season] += 1;
    }
  });
  return matchesCountPerYear;
};

module.exports = numberOfMatchesPlayedPerYear;
