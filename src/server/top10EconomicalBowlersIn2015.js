const topEconomicalBowlersInYear = (matches, deliveries, year) => {

  const getFirstAndLastMatchId = (matches, year) => {
    const matchesPlayedInYear = matches.filter((match) => parseInt(match.season) === year);
    let firstMatchId = parseInt(matchesPlayedInYear[0].id);
    let lastMatchId = parseInt(matchesPlayedInYear[matchesPlayedInYear.length - 1].id);

    return { firstMatchId, lastMatchId };
  };

  const findingBowlerDetails = (deliveries, firstMatchId, lastMatchId) => {
    let bowlerDetails = {};

    deliveries.filter((delivery) => parseInt(delivery.match_id) >= firstMatchId &&
      parseInt(delivery.match_id) <= lastMatchId)
      .forEach((delivery) => {
        let bowler = delivery.bowler;
        let runsConceded = parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs) + parseInt(delivery.batsman_runs);
        let isBallLegit = parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs) == 0;

        if (!bowlerDetails.hasOwnProperty(bowler)) {
          bowlerDetails[bowler] = {};
          bowlerDetails[bowler]["name"] = bowler;
          bowlerDetails[bowler]["runsConceded"] = runsConceded

          if (!bowlerDetails[bowler].hasOwnProperty("ballsBowled")) {
            bowlerDetails[bowler]["ballsBowled"] = 0;
            if (isBallLegit) {
              bowlerDetails[bowler]["ballsBowled"] = 1;
            }
          }
        }
        else {
          bowlerDetails[bowler]["runsConceded"] += runsConceded;

          if (isBallLegit) {
            bowlerDetails[bowler]["ballsBowled"] += 1;
          }
        }
        let totalRunsConceded = bowlerDetails[bowler]["runsConceded"];
        let totalBallsBowled = bowlerDetails[bowler]["ballsBowled"];
        let economy = ((totalRunsConceded / totalBallsBowled) * 6).toFixed(2)
        bowlerDetails[bowler]["Economy"] = parseFloat(economy);
      });

    return bowlerDetails;
  };

  const sortingBowlerDetailsbasedOnEconomy = (bowlerDetails) => {
    const bowlerDetailsArray = Object.values(bowlerDetails);
    bowlerDetailsArray.sort((a, b) => a.Economy - b.Economy);
    return bowlerDetailsArray;
  };

  let { firstMatchId, lastMatchId } = getFirstAndLastMatchId(matches, year);
  const bowlerDetails = findingBowlerDetails(
    deliveries,
    firstMatchId,
    lastMatchId
  );

  const sortedArray = sortingBowlerDetailsbasedOnEconomy(bowlerDetails);
  return (sortedArray.slice(0, 10));
};
module.exports = topEconomicalBowlersInYear;
