const csv = require("csvtojson");
const fs = require("fs");

const matches = "../data/matches.csv";
const deliveries = "../data/deliveries.csv";
const numberOfMatchesPlayedPerYear = require("./numberOfMatchesPlayedPerYear");
const numberOfMatchesWonPerTeamPerYearYear = require("./numberOfMatchesWonPerTeamPerYearYear");
const extraRunsConcededPerTeamInYear = require("./extraRunsConcededPerTeamIn2016.js");
const topEconomicalBowlersInYear = require("./top10EconomicalBowlersIn2015.js");
const getStrikeRates = require("./getStrikeRate.js");
const findNoOfTimesTeamWonTossAndGame = require("./findNoOfTimesTeamWonTossAndGame.js")
const findingPlayerWithMostMOMPerYear = require("./findingPlayerWithMostMOMPerYear.js")
const getStrikerateOfBatsmenPerSeason = require("./getStrikerateOfBatsmenPerSeason")
const findingBestEconomyBowlerInSuperOvers = require("./findingBestEconomyBowlerInSuperOvers.js");
const getHighestNoOfTimePlayerDismissedByOther = require("./highestNoOfTimePlayerDismissedByOther.js")

csv()
  .fromFile(matches)
  .then((matches) => {

    let result1 = numberOfMatchesPlayedPerYear(matches);
    result1 = JSON.stringify(result1);

    fs.writeFile(
      "../public/output/matchesPerYear.json",
      result1,
      "utf8",
      function (err) {
        if (err) {
          console.log("An error occured while writing JSON Object to File.");
          return console.log(err);
        }
      }
    );

    let winsPerTeamPerYear = numberOfMatchesWonPerTeamPerYearYear(matches);
    winsPerTeamPerYear = JSON.stringify(winsPerTeamPerYear);
    fs.writeFile(
      "../public/output/winsPerTeamPerYear.json",
      winsPerTeamPerYear,
      "utf8",
      (err) => {
        if (err) {
          console.log("An error occured while writing JSON Object to File.");
          return console.log(undefined);
        }
      }
    );


    let numberOfTeamsWonTossAndGame = findNoOfTimesTeamWonTossAndGame(matches)
    numberOfTeamsWonTossAndGame = JSON.stringify(numberOfTeamsWonTossAndGame)

    fs.writeFile(
      "../public/output/numberOfTeamsWonTossAndGame.json",
      numberOfTeamsWonTossAndGame,
      "utf8",
      (err) => {
        if (err) {
          console.log("An error occured while writing JSON Object to File.");
          return console.log(undefined);
        }
      }
    );

    let playersWithMostMOM = findingPlayerWithMostMOMPerYear(matches);
    playersWithMostMOM = JSON.stringify(playersWithMostMOM)

    fs.writeFile("../public/output/playersWithMostPlayerOfMatch.json", playersWithMostMOM, "utf-8", (err) => {
      if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(undefined);

      }
    })
    csv()
      .fromFile(deliveries)
      .then((deliveries) => {
        let year = 2016;
        let extraRunsDetails = extraRunsConcededPerTeamInYear(matches, deliveries, year);
        extraRunsDetails = JSON.stringify(extraRunsDetails);
        //console.log(extraRunsDetails)
        fs.writeFile(
          "../public/output/extraRunsConcededPerTeamIn2016.json",
          extraRunsDetails,
          (err) => {
            if (err) {
              console.log("An error occurred while writting JSON Object to File");
              return console.log(undefined);
            }
          }
        );
        year = 2015;
        let top10EconomicalBowlers = topEconomicalBowlersInYear(
          matches,
          deliveries, year
        );
        top10EconomicalBowlers = JSON.stringify(top10EconomicalBowlers);
        // console.log(top10EconomicalBowlers)
        fs.writeFile(
          "../public/output/top10EconomicalBowlersIn2015.json",
          top10EconomicalBowlers,
          (err) => {
            if (err) {


              console.log("An Error occurred while writing JSON Object to file");

              return console.log(undefined);
            }
          }
        );

        let strikeRatesOfBatsmenPerSeason = getStrikerateOfBatsmenPerSeason(matches, deliveries)
        strikeRatesOfBatsmenPerSeason = JSON.stringify(strikeRatesOfBatsmenPerSeason)
        fs.writeFile("../public/output/strikeRatesOfBatsmanPerYear.json", strikeRatesOfBatsmenPerSeason, (err) => {
          if (err) {
            console.log("An Error occurred while writing JSON Object to file");

            return console.log(undefined);
          }
        })


        let bestBowlerInSuperOvers = findingBestEconomyBowlerInSuperOvers(deliveries)
        bestBowlerInSuperOvers = JSON.stringify(bestBowlerInSuperOvers)
        fs.writeFile("../public/output/bestBowlerInSuperOvers.json", bestBowlerInSuperOvers, (err) => {
          if (err) {
            console.log("An Error occurred while writing JSON Object to file");

            return console.log(undefined);
          }
        })
        let strikerateOfBatsmenPerSeason = getStrikerateOfBatsmenPerSeason(matches, deliveries)
        strikerateOfBatsmenPerSeason = JSON.stringify(strikerateOfBatsmenPerSeason)
        fs.writeFile("../public/output/strikeRatesOfBatsmanPerYear.json", strikerateOfBatsmenPerSeason, (err) => {
          if (err) {
            console.log("An Error occurred while writing JSON Object to file");

            return console.log(undefined);
          }
        })

        let highestNoOfTimePlayerDismissedByOther = getHighestNoOfTimePlayerDismissedByOther(deliveries)
        highestNoOfTimePlayerDismissedByOther = JSON.stringify(highestNoOfTimePlayerDismissedByOther)
        fs.writeFile("../public/output/highestNoOfTimePlayerDismissedByOther.json", highestNoOfTimePlayerDismissedByOther, (err) => {
          if (err) {
            console.log("An Error occurred while writing JSON Object to file");

            return console.log(undefined);
          }
        })
        let playerName = "SK Raina"

        let strikeRateAnalysis = getStrikeRates(deliveries, playerName)
        strikeRateAnalysis = JSON.stringify(strikeRateAnalysis)
        fs.writeFile("../public/output/strikeRateAnalysis.json", strikeRateAnalysis, (err) => {
          if (err) {
            console.log("An Error occurred while writing JSON Object to file");

            return console.log(undefined);
          }
        })

      });

  });
