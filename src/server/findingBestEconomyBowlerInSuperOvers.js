const findingBestEconomyBowlerInSuperOvers = (deliveries) => {
    let bowlerDetails = {}

    deliveries.filter((delivery) => { return delivery.is_super_over == 1 })
        .forEach(delivery => {
            let bowler = delivery.bowler
            let runsConceded = parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs) + parseInt(delivery.batsman_runs);
            let isBallLegit = parseInt(delivery.wide_runs) + parseInt(delivery.noball_runs) == 0;

            if (!bowlerDetails.hasOwnProperty(bowler)) {
                bowlerDetails[bowler] = {};
                bowlerDetails[bowler]["name"] = bowler;
                bowlerDetails[bowler]["runsConceded"] = runsConceded

                if (!bowlerDetails[bowler].hasOwnProperty("ballsBowled")) {
                    bowlerDetails[bowler]["ballsBowled"] = 0;
                    if (isBallLegit) {
                        bowlerDetails[bowler]["ballsBowled"] = 1;
                    }
                }
            }
            else {
                bowlerDetails[bowler]["runsConceded"] += runsConceded;

                if (isBallLegit) {
                    bowlerDetails[bowler]["ballsBowled"] += 1;
                }
            }
            let totalRunsConceded = bowlerDetails[bowler]["runsConceded"];
            let totalBallsBowled = bowlerDetails[bowler]["ballsBowled"];
            let economy = ((totalRunsConceded / totalBallsBowled) * 6).toFixed(2)
            bowlerDetails[bowler]["Economy"] = parseFloat(economy);
        });


    const bowlersArray = Object.values(bowlerDetails)
    bowlersArray.sort((a, b) => (a["Economy"] - b["Economy"]))
    return bowlersArray[0]
}
module.exports = findingBestEconomyBowlerInSuperOvers 