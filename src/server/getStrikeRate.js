const getStrikeRates = (deliveries, playerName) => {


    const getStrikeRate = (delivery, obj) => {

        if (parseInt(delivery.over) <= 6 && parseInt(delivery.is_super_over) !== 1) {
            obj.powerPlayRuns += parseInt(delivery.batsman_runs);
            if (parseInt(delivery.wide_runs) === 0) {
                obj.powerPlayBalls += 1
            }
        } else if (parseInt(delivery.over) >= 6 && parseInt(delivery.over) <= 14) {
            obj.middleOverRuns += parseInt(delivery.batsman_runs);
            if (parseInt(delivery.wide_runs) === 0) {
                obj.middleOverBalls += 1
            }
        } else if (parseInt(delivery.over) <= 20 && parseInt(delivery.over) >= 14) {
            obj.slogOverRuns += parseInt(delivery.batsman_runs);
            if (parseInt(delivery.wide_runs) === 0) {
                obj.slogOverBalls += 1
            }
        } else if (parseInt(delivery.is_super_over) !== 0) {
            obj.superOverRuns += parseInt(delivery.batsman_runs);
            if (parseInt(delivery.wide_runs) === 0) {
                obj.superOverBalls += 1
            }
        }
        return obj
    }

    let playerDetails = { name: playerName, powerPlay: 0, middleOvers: 0, slogOvers: 0, superOver: 0 }
    let obj = {
        powerPlayRuns: 0, powerPlayBalls: 0, middleOverRuns: 0, middleOverBalls: 0, slogOverRuns: 0, slogOverBalls: 0, superOverRuns: 0, superOverBalls: 0
    }

    deliveries.filter(delivery => delivery.batsman === playerName)
        .forEach(delivery => {
            obj = getStrikeRate(delivery, obj);

            playerDetails.powerPlay = parseFloat(((obj.powerPlayRuns / obj.powerPlayBalls) * 100).toFixed(2))
            playerDetails.middleOvers = parseFloat(((obj.middleOverRuns / obj.middleOverBalls) * 100).toFixed(2))
            playerDetails.slogOvers = parseFloat(((obj.slogOverRuns / obj.slogOverBalls) * 100).toFixed(2))
            playerDetails.superOver = parseFloat(((obj.superOverRuns / obj.superOverBalls) * 100).toFixed(2))

            for (let key in playerDetails) {
                if (!playerDetails[key]) {
                    playerDetails[key] = 0
                }
            }

        })
    return playerDetails

}

module.exports = getStrikeRates