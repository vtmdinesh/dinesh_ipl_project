const { parseSubArray } = require("convert-csv-to-json/src/csvToJson")

const findingPlayerWithMostMOMPerYear = (matches) => {

    let seasonList = []
    let playerOfMatchDetails = {}
    matches.forEach(match => {
        if (!seasonList.includes(parseInt(match.season))) {
            seasonList.push(parseInt(match.season))
            seasonList.sort((a, b) => a - b)
        }
    })



    seasonList.forEach(season => {
        matches.filter(match => parseInt(match.season) == season)
            .forEach(match => {

                if (!playerOfMatchDetails.hasOwnProperty(season)) {
                    playerOfMatchDetails[season] = {}
                    if (!playerOfMatchDetails[season].hasOwnProperty(match.player_of_match
                    )) {
                        playerOfMatchDetails[season][match.player_of_match] = 1
                    }
                    else {
                        playerOfMatchDetails[season][match.player_of_match] += 1
                    }
                } else {
                    if (!playerOfMatchDetails[season].hasOwnProperty(match.player_of_match
                    )) {
                        playerOfMatchDetails[season][match.player_of_match] = 1
                    }
                    else {
                        playerOfMatchDetails[season][match.player_of_match] += 1
                    }
                }

            })
    })
    let playersWithMostMOM = {}
    for (let season in playerOfMatchDetails) {
        let momDetails = Object.entries(playerOfMatchDetails[season])
        momDetails.sort((a, b) => b[1] - a[1])
        let playerWithMostMOM = Object.fromEntries([momDetails[0]])
        playersWithMostMOM[season] = playerWithMostMOM
    }
    return playersWithMostMOM

}

module.exports = findingPlayerWithMostMOMPerYear