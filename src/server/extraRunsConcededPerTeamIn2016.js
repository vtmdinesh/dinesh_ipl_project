const extraRunsConcededPerTeamInYear = (matches, deliveries, year) => {
  const matchesPlayedInYear = matches.filter(
    (match) => parseInt(match.season) === year
  );
  const firstMatchId = parseInt(matchesPlayedInYear[0].id);
  const lastMatchId = parseInt(
    matchesPlayedInYear[matchesPlayedInYear.length - 1].id
  );
  let extraRunsDetails = {};

  deliveries
    .filter(
      (delivery) =>
        parseInt(delivery.match_id) >= firstMatchId &&
        parseInt(delivery.match_id) <= lastMatchId
    )
    .forEach((delivery) => {
      let bowlingTeam = delivery.bowling_team;

      if (!extraRunsDetails.hasOwnProperty(bowlingTeam)) {
        extraRunsDetails[bowlingTeam] = parseInt(delivery.extra_runs);
      } else {
        extraRunsDetails[bowlingTeam] += parseInt(delivery.extra_runs);
      }
    });

  return extraRunsDetails;
};

module.exports = extraRunsConcededPerTeamInYear;
