const numberOfMatchesWonPerTeamPerYearYear = (matches) => {
  let matchesWonPerYearPerTeam = {};
  let sortedMatchesWonPerYearPerTeam = {};

  matches.forEach((match) => {
    let winner = match.winner;
    let season = match.season;

    if (winner === "") {
      winner = "No Result";
    }

    if (!matchesWonPerYearPerTeam.hasOwnProperty(winner)) {
      matchesWonPerYearPerTeam[winner] = {};
    }

    if (!matchesWonPerYearPerTeam[winner].hasOwnProperty(season)) {
      matchesWonPerYearPerTeam[winner][season] = 1;
    } else {
      matchesWonPerYearPerTeam[winner][season] += 1;
    }
  });

  let objectArray = Object.entries(matchesWonPerYearPerTeam);
  let sortedArr = objectArray.sort();

  sortedArr.forEach((team) => {
    sortedMatchesWonPerYearPerTeam[team[0]] = team[1];
  });

  return sortedMatchesWonPerYearPerTeam;
};

module.exports = numberOfMatchesWonPerTeamPerYearYear;
